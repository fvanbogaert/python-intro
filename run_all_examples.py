#!/usr/bin/env python3
import glob
import os, sys

print(sys.argv)

for example in sorted(glob.glob("root/examples/*.py")):
    print("Running example {}".format(example[:-2]))
    os.system("{} {}".format("python3", example))
