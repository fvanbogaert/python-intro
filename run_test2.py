#!/usr/bin/env python3
from tutorial.tests.test_04 import suite
import unittest


runner = unittest.TextTestRunner()
runner.run(suite())
