#!/usr/bin/env python3
from tutorial.examples.intro_08_classes import Image
import unittest

testdata = [
    [1, 2, 3],
    [77, 66, 55],
    [44, 45, 46]
]


class ImageTest(unittest.TestCase):

    def setUp(self):
        self.img = Image(testdata)

    def test_initial_data(self):
        self.assertTrue(self.img.get_pixel(0, 0) == 1, "Pixel data was not copied correctly")
        self.assertEqual(self.img.get_pixel(1, 1), 66, "Pixel data was not copied correctly")
        self.assertEqual(self.img.get_pixel(2, 2), 46, "Pixel data was not copied correctly")

    def test_set_pixel(self):
        self.assertEqual(self.img.get_pixel(1, 1), 66, "Pixel data was not copied correctly")
        self.img.set_pixel(2, 2, 55)
        self.assertEqual(self.img.get_pixel(2, 2), 46, "Pixel data was not set correctly")
        with self.assertRaises(IndexError):
            self.img.set_pixel(3, 3, 55)

    def test_dimensions(self):
        self.assertEqual(self.img.height(), 3, "Image height is incorrect")
        self.assertEqual(self.img.width(), 3, "Image width is incorrect")

if __name__ == '__main__':
    unittest.main()
