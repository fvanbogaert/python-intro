#!/usr/bin/env python3
import unittest
import os
import os.path
from tutorial.examples.example_02_tempfile import TempFile


class CommonBase(unittest.TestCase):
    filename = 'test.file'
    teststring = "hello, world"
    def setUp(self):
        self.tempfile = TempFile(filename=self.filename)

    def tearDown(self):
        del self.tempfile
        assert not os.path.exists(self.filename), "File cleanup failed"


class TempFileWriterTests(CommonBase):

    def test_basic_write(self):
        self.tempfile.write(self.teststring)
        self.tempfile.f.flush()
        with open(self.filename) as f:
            contents = f.read()
            self.assertEqual(contents, self.teststring)


class TempFileReaderTests(CommonBase):

    def test_basic_read(self):
        for i in range(3):
            self.tempfile.write("%s%d\n" % (self.teststring, i))
        for i in range(3):
            line = self.tempfile.readline()
            self.assertEqual(line, "%s%d\n" % (self.teststring, i), "Line read #{} failed!".format(i))


def suite():
    suite = unittest.TestSuite()
    suite.addTest(TempFileWriterTests('test_basic_write'))
    suite.addTest(TempFileReaderTests('test_basic_read'))
    return suite


if __name__ == '__main__':
    unittest.main()




