#!/usr/bin/env python3
from tutorial.examples.intro_08_classes import Image

testdata = [
    [1, 2, 3],
    [77, 66, 55],
    [44, 45, 46]
]


def main():
    img = Image(testdata)
    assert img.get_pixel(0, 0) == 1, "Pixel data was not copied correctly"
    assert img.get_pixel(1, 1) == 66, "Pixel data was not copied correctly"
    assert img.get_pixel(2, 2) == 46, "Pixel data was not copied correctly"

    img.set_pixel(2, 2, 55)
    assert img.get_pixel(2, 2) == 46, "Pixel data was not set correctly"

    assert img.height() == 3, "Image height is incorrect"
    assert img.width() == 3, "Image width is incorrect"

    print("Everything works!")


if __name__ == '__main__':
    main()
