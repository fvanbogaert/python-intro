#!/usr/bin/env python3

"""
This example illustrates the following new concepts:
* Using numbers in python
* Using variables in python
* Combining variables in python
* Addition, raising to the power, division, integer division
* Using print arguments to print values

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""


def main():
    # We assign the value 2 to the variable called a.
    # The type (int) is automatically determined in python, as in most scripting languages
    a = 2
    # We assign the value 3 to the variable called b
    b = 3
    # We assign the result of adding a (2) to b (3) in the variable called c.
    # It follows that the value will be 5
    c = a + b
    # We raise the value of a (2) to the power of 3, and add the value of b.
    # The result is stored in a variable called d
    d = a**3 + b
    # The value of b divided by a will be stored in the variable e.
    # Note that in python 3 (in contrast to python 2) the result of a division is always a float
    #   even if the arguments are integers. So the result is always exact.
    # The value of 'e' will be 1.5 in this case.
    e = b / a
    # Use 'integer division' instead. This results in an integer (it rounds down).
    # The result stored in f will be 1
    f = b // a

    # This is another form of the print statement. You can add as many arguments as you like,
    #   and python will add a space between them.
    # print also always adds a newline character,
    #   unless you use an option to override that behaviour
    # That means you don't see "a is 2b is 3c is..." etc all on one line,
    #   but each print will be on a separate line.
    print("a is", a)
    print("b is", b)
    print("c is", c)
    print("d is", d)
    print("e is", e)
    print("f is", f)


main()
