#!/usr/bin/env python3
import os

class TempFile:

    index = 0
    linenr = 0

    def __init__(self, filename=None, encoding=None):

        if filename is None:
            filename = '/tmp/dummy-{}'.format(self.index)
            self.index += 1
        self.filename = filename

        if encoding is None:
            encoding = 'utf-8'
        self.encoding = encoding

        self.f = open(filename, 'w+', encoding=encoding)

    def __del__(self):
        self.f.close()
        os.remove(self.filename)

    def write(self, contents: str):
        print(contents, file=self.f, end='')

    def readline(self) -> str:
        if self.linenr == 0:
            self.f.seek(0)
        self.linenr += 1
        return self.f.readline()
