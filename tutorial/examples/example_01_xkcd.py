#!/usr/bin/env python3

from PySide2.QtWidgets import QApplication, QLabel, QWidget, QVBoxLayout, QHBoxLayout, QPushButton
from PySide2.QtGui import QPixmap
from PySide2.QtCore import Slot, Qt
import sys
import requests
import tempfile

class MyWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.prev = QPushButton("Previous")
        self.image = QLabel()
        self.image.setAlignment(Qt.AlignCenter)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.image)
        self.layout.addWidget(self.prev)
        self.setLayout(self.layout)

        # Connecting the signal
        self.prev.clicked.connect(self.loadPrev)

        self.num = 0
        self.getImage()

    def getImage(self):
        if not self.num:
            current_comic_url = 'http://xkcd.com/info.0.json'
        else:
            current_comic_url = 'http://xkcd.com/{}/info.0.json'.format(self.num)
        request = requests.get(current_comic_url)
        if not request.ok:
            raise Exception("Failed to download comic meta-data")

        # The 'requests' library has a built-in JSON parser.
        # We can use that to extract all the useful data as a python dictionary
        comic_data = request.json()
        self.num = comic_data['num']
        self.setWindowTitle(comic_data['title'])
        self.image.setToolTip(comic_data['alt'])

        dl = requests.get(comic_data['img'])
        if not dl.ok:
            raise Exception("Failed to download comic image")

        with tempfile.NamedTemporaryFile() as comic:
            comic.write(dl.content)
            comic.flush()
            pixmap = QPixmap(comic.name)
            self.image.setPixmap(pixmap)

    @Slot()
    def loadPrev(self):
        self.num -= 1
        self.getImage()

if __name__ == "__main__":
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.resize(800, 600)
    widget.show()

    sys.exit(app.exec_())
