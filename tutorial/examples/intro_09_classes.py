#!/usr/bin/env python3
from intro_08_classes import Image

"""
This example illustrates the following new concepts:
* Using code defined in another file
* Class inheritance in python
* Operator overloading in python
* Multiple return values
* List comprehensions

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""

class Img(Image):
    def __get__(self, s):
        return self.pixels[s]

    # Note here that I've added an annotation signifying what this function will return.
    # In this case, this function will return a tuple of 3 integers.
    # A tuple in python is a read-only list.
    # As you may remember, we define a normal list in python with square brackets: l = [1, 2, 3]
    # A tuple is formed using smooth brackets: t = (1, 2, 3).
    # They are optional in some cases: the previous example can be written as t = 1, 2, 3
    def get_brightest_pixel(self) -> (int, int, int):
        brightest = 0
        row = 0
        col = 0
        for rownr, line in enumerate(self.pixels):
            for colnr, pixel in enumerate(line):
                if pixel > brightest:
                    brightest = pixel
                    row = rownr
                    col = colnr
        rv = (row, col, brightest)
        return rv


def main():
    p = [   [10, 11, 12],
            [40, 41, 42],
            [20, 21, 22],
            [0,  50,  0]]
    i = Img(p)
    x, y, v = i.get_brightest_pixel()
    print("The brightest pixel is located at ({x},{y}) and has value {v}".format(x=x, y=y, v=v))



if __name__ == '__main__':
    main()
