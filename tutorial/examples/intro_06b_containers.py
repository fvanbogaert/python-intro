#!/usr/bin/env python3
"""
This example illustrates the following new concepts:
* The 3 types of containers in base python
* How they are used
* That you can also index strings the same way

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""

# Python has 3 kinds of general-purpose container built-in:
# * tuple
# * list (a.k.a. array)
# * dict (a.k.a. dictionary a.k.a. map)
#
# In addition, there are also more containers in the standard library:
# import collections
# collections.namedtuple
# collections.deque
# collections.defaultdict
# ...
#
# A tuple is a read-only list: the list cannot be changed, and elements of the list can't either.
# They are useful for lists of constants, as return values, or for ensuring that items don't change.
# It's defined using round brackets (which are optional):

my_tuple = (1, 2, 3)
my_tuple_2 = "A", "B", "C"

print(my_tuple[1])  # Prints 2
#my_tuple[0] = 0 # This is not allowed
#my_tuple += (4, 5, 6) # This is not allowed

# Tuples can be 'unpacked' easily:
a, b, c = my_tuple  # a=1 b=2 c=3
d, e, f, _ = ("Cool", "Data", "Type", "Bro")  # d="Cool", e="Data", f="Type". _ means ignore

# Lists, as previously mentioned, can be written to, expanded, and have their elements changed
# They are defined using square brackets
my_list = [1, 2, 3]
my_list_2 = ["A", "B", "C"]

print(my_list[1])
my_list[0] = 55
my_list += [4, 5, 6]   # my_list is now [55, 2, 3, 4, 5, 6]

# Both tuples and lists can be 'sliced':
my_tuple[1:3]   # (2, 3)
my_list[2:5]   # [3, 4, 5]
my_list[-2:]   # [5, 6]
my_list[:-3]   # [55, 2, 3]

# Next up is the dict(ionary) type. It's a key=> value mapping and uses curly brackets:
my_dict = {
    'A': 1,
    'B': 2,
    'D': 4,
    'E': 5
}

print(my_dict['B'])     # Prints 2
# You can override values at will
my_dict['A'] = 0
# You can add to a dictionary by assigning to new keys
my_dict['F'] = 6
# You can get just the keys or just the values of a dict:
my_dict_keys = my_dict.keys()
my_dict_values = my_dict.values()
# You can convert a dict to and from a list of tuples
my_dict_items = my_dict.items()  # [('A', 0), ('B', 2), ('D', 4), ('E', 5), ('F', 6)]
my_dict_copy = dict(my_dict_items)

# Iterating over all types in a for loop:

for item in my_tuple:
    print(item)

for item in my_list:
    print(item)

for key in my_dict:
    print("{}: {}".format(key, my_dict[key]))

# Alternatively
for key, value in my_dict.items():
    print("{}: {}".format(key, value))

# Also, the 'enumerate' keyword is helpful for numbering items
for index, item in enumerate(my_list):
    print("Item #{}: {}".format(index, item))

# Finally, strings in python can also be indexed and sliced like tuples or lists
my_string = 'This is a \'really\' "nice" string'
print(my_string[2:4])   # "CD"
# A character is just a string with length one
my_character = 'C'
print(my_character[0])  # "C"

# Single and double quotes are equivalent in python.
# You can also use the form with 3 quotes (single or double) for multi-line strings
my_long_string = """
Hello '''test'''
World
"""
print(my_long_string)
