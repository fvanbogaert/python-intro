#!/usr/bin/env python3

"""
This example illustrates the following new concepts:
* running python in a file on a unix-like platform
* using the shebang line
* using the print function
* Using pydoc for commenting a file

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""


# This is a comment. Python ignores this, so it's just meant for humans.
# That means I can tell you about things in this code close to this comment.
# Comments in python always start with # and last until the end of the line.
# You can also do multi-line comments with """ as you might have noticed, above.
# That second style is called a 'docstring'.
# Python actually keeps it arnound as the documentation of the code block in question,
#  which can make documenting code easy


# This 'prints' the line "Hello, world" on the terminal (the "standard output" of the program)
# print is a standard function in python that is called from the script to perform this task
# It can do other cool things too, as we'll see later.
print("Hello, world!")
