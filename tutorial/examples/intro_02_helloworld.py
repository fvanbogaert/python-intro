#!/usr/bin/env python3

"""
This example illustrates the following new concepts:
* Writing a function in python
* Calling a function in python
* The general principle of wrapping functionality in functions as a Good Idea (tm)

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""

# The 'def' keyword marks this as a function definition.
# We're writing some code here that is not executed immediately.
# It needs to be 'called' first


def main():
    print("Hello, world!")


# Here, we're calling the main function we made earlier
main()
