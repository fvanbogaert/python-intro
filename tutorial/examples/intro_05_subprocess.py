#!/usr/bin/env python3
import os

"""
This example illustrates the following new concepts:
* Writing a function that takes an argument
* import can be used in a function
* Splitting a string to obtain a list
* Using the subprocess module to launch a subprocess
* Using os.system to launch a subprocess
* Using named arguments (when calling a function)
* assigning the result of a function call
* Decoding a byte array to a string (and the difference between each in python)

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""


# Note the (cmd) part. This means, I expect an argument to this function, which I shall call cmd.
def start(cmd):
    # You can import modules within functions, like this.
    # It's probably not a good idea in most cases, though,
    #   as it may cause a lot of code to be executed again and again (instead of just once).
    import subprocess

    # Subprocess.run expects to get a list of arguments, instead of having them all in one string
    # So, it wants a list like ['ping', '-c', '3', '8.8.8.8']
    # We can convert from the string form to the list form
    #   by splitting the string into pieces based on spaces
    args = cmd.split(' ')

    # This function will run the command,
    #   and give us an object back with the result of running the command!
    # Note that we explicitly tell subprocess.run that we want to capture the output.
    #   This is a named, optional argument to subprocess.run
    #   (we did not have to specify it to run the function, but then we wouldn't get the output).
    # The result is an 'object', which we'll talk about later.
    # It's basically a container that holds all the information we're interested in.
    result = subprocess.run(args, capture_output=True)

    # result.stdout (the stdout variable from within result) contains the output of our subprocess.
    # This means we can print it!
    # However, if you print it, it will look ugly.
    # That's because what we actually got was not a string, but a 'byte array'
    # In C they're basically the same thing, but python distinguishes between the two.
    print(result.stdout)
    print()
    # We can convert the byte array to a string by using the decode function on it.
    # This means it looks a lot nicer when we print it :-)
    # Note here that I'm assuming the text is formatted in UTF-8, which is *usually* the case.
    # If it isn't, python could mis-read some of the bytes, and it will look like gibberish, or
    #   python could notice that it's incorrect and refuse to continue
    #    (raise an Exception, which we'll talk about later).
    # Converting things to string helps with more than just printing it,
    #      it makes a lot of operations easier.
    print(result.stdout.decode('utf-8'))

    # The result object also contains the process return code.
    return result.returncode


def main():
    os.system("echo 'hello world'")
    result = start("ping -c 3 8.8.8.8")
    print("result code = {}".format(result))


main()
