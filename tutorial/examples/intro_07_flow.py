#!/usr/bin/env python3
from random import random
import sys

"""
This example illustrates the following new concepts:
* 'from module import item' syntax
* random numbers in python
* if statements
* enumerate
* growing arrays
* "string" in string
* while loops
* basic exception handling
* if ... elif ... else

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""




def main():


    questions = [   "What... is your name?",\
                    "What... is your quest?",\
                    "What... is your favorite colour?"
    ]
    if random() < 0.5:
        questions[2] = "What ... is the capital of Assyria?"
    answers = []

    for index, question in enumerate(questions):
        print()
        print(question)

        answer = sys.stdin.readline().strip()

        #answers = answers + [answer]
        answers += [answer]
        #answers.append(answer)

        if not answer or "don't know" in answer:
            print(".... AAAAAARRRRGH")
            print("... you died. Better luck next time!")
            sys.exit(1)

    print("Your answers were:", answers)

    valid = False
    speed = 0
    #while valid == False:
    #while valid != True:
    #while valid is False:
    #while valid is not True:
    #while valid is not not False:
    while not valid:
        print("What's the average airspeed of an unladen swallow? (in km/h)")
        try:
            answer = sys.stdin.readline().strip()
            speed = int(answer)
            valid = True
        except:
            print("Please reply with a number")

    if speed < 0:
        print("What, they fly backwards?")
    elif speed < 30:
        print("That seems kind of slow ...")
    elif speed < 200:
        print("Seems legit.")
    else:
        print("Wait, did the swallow board a supersonic jet plane?")


if __name__ == '__main__':
    main()
