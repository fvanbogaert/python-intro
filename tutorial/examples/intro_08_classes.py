#!/usr/bin/env python3


"""
This example illustrates the following new concepts:
* Class definition
* member functions
* docstrings
* raising exceptions
* catching a specific exception
* multi-dimensional arrays
* a first hint about modules

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""


class Image:
    ''' a two-dimensional pixel container '''

    def __init__(self, pixels):
        """Create the image. You have to provide the pixels yourself, though!"""
        self.pixels = pixels

    def width(self):
        '''Returns the width of the image'''
        return len(self.pixels[0])

    def height(self):
        '''Returns the height of the image'''
        return len(self.pixels)

    def get_pixel(self, x, y):
        '''
        Get the pixel at the (x, y) coordinate specified.
        @returns the pixel at position (x, y)
        @throws IndexError if the x, y value exceeded the bounds.
        '''
        if x >= self.width():
            raise IndexError("x value too large: {} vs {}".format(x, self.width()))
        if y >= self.height():
            raise IndexError("y value too large: {} vs {}".format(y, self.height()))

        return self.pixels[x][y]

    # Note the 'int' here: it's a hint you can put in your code that you expect a certain type of argument
    # in this case, an integer.
    def set_pixel(self, x, y, value: int):
        '''set the pixel at coordinates (x,y) to a certain value.'''
        if x >= self.width():
            raise IndexError("x value too large: {} vs {}".format(x, self.width()))
        if y >= self.height():
            raise IndexError("y value too large: {} vs {}".format(y, self.height()))

        self.pixels[x][y] = value


def main():
    # We're defining a 2-dimensional array here, also known as a matrix
    # Please pretend that these are pixel values. We're using simple numbers here;
    # in a real implementation, these would probably be RGB values
    # For this example, I've chosen a 4x3 array (4 columns wide, 3 rows tall)
    p = [
        [ 2, 3, 4, 5],
        [ 5, 6, 7, 8],
        [11, 1, 3, 2]]
    # Initialize our 'Image' object with the 'pixels' we defined above
    i = Image(p)

    print("Image width={} and height={}".format(i.width(), i.height()))
    print("pixel at (1,1) is {}".format(i.get_pixel(1, 1)))
    i.set_pixel(1, 1, 55)
    print("pixel at (1,1) is {}".format(i.get_pixel(1, 1)))
    print("get_pixel documentation:", i.get_pixel.__doc__)

    try:
        i.get_pixel(55, 55)
        i.set_pixel(0, 0, 20)
    except IndexError as e:
        print(e)

    print("pixel at (0, 0) is", i.get_pixel(0, 0))


if __name__ == '__main__':
    main()
