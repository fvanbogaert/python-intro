#!/usr/bin/env python3
import sys

"""
This example illustrates the following new concepts:
* Defining a custom list in python
* 'for' loops
* The 'range' generator function
* Indexing a list
* Assigning a value of a list
* iterating over a list in 3 different ways
* reading a line of standard input

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""


def main():

    # range is a special function, built-in to python.
    # It returns an object that generates numbers up to (but excluding) the argument you specified.
    # range(4) will generate the numbers 0, 1, 2, 3
    # If you want a different start point, you can specify two arguments to range: a start and end
    #   range(3, 8) generates the numbers 3, 4, 5, 6, 7 for instance (8 is not generated, as before).
    #
    # for is a keyword that indicates that this is a for loop: we will keep doing the thing inside the for
    #   (which is the print call) so long as we don't run out of numbers.
    #   This means we will do it 4 times (as the range only generates 4 numbers, in this case).
    for index in range(4):
        print(index)

    # This is a list in python, which is a collection of sub-items.
    # Note the square brackets, and the commas between elements.
    numbers = [33, 44, 55, 67]

    # We can print out the elements of the list like this:
    print(numbers[0])
    print(numbers[1])
    print(numbers[2])
    print(numbers[3])

    # You can change an element of the list like this
    numbers[3] = 66

    # We can combine this with the for and the range function to iterate over all the numbers:
    for index in range(4):
        # in the first time around, index is 0. So this is print(numbers[0]).
        # The next time, it is 1, so this becomes print(numbers[1]). Etc ...
        print(numbers[index])

    # The previous for loop had a problem: if we change the size of our list,
    #   we won't be printing the correct amount of numbers anymore!
    # We want to print exactly the amount of elements the list has.
    #   The 'len' built-in function gives us the number of elements.
    #   range(len(numbers)) will generate exactly as many numbers as there are in the numbers list,
    #       if you change the list, this will still work correctly:
    for index in range(len(numbers)):
        print(numbers[index])

    # This is a simpler way of writing the above:
    #   we can just iterate over the numbers list directly.
    # Why didn't I do this in the first place? Sometimes you need the other ways of doing it.
    #   For instance, if you want to print "number 0 is 33", "number 1 is 44" etc
    for number in numbers:
        print(number)

    # We can also iterate over a list of strings
    questions = ["What... is your name?", "What... is your quest?", "What... is your favorite colour?"]

    for question in questions:
        print()
        # Ask your questions, bridgekeeper, I'm not afraid!
        print(question)

        # This is how to get input from the console keyboard back into python. It reads a whole line at once
        answer = sys.stdin.readline()

        # The line comes with a newline character at the end, though. We don't want that in this case, so the
        #   'strip' function can remove it from us. It removes all whitespace at both ends of the string.
        answer = answer.strip()

        print("You answered: '{}'".format(answer))

    print("You may pass")


main()
