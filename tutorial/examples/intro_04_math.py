#!/usr/bin/env python3
import math

"""
This example illustrates the following new concepts:
* The import statement
* Using a function imported from a module
* The sin(), cos(), log() and sqrt() functions from math (along with math.pi)
* Formatting of values using printf-style syntax in python
* Formatting of values using format()
* Functions can be called with either variables or expressions, depending on taste

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""


def main():
    # So, what's sin(pi/2) again? Why not calculate it with python!
    # The '%f' is printf syntax, the same (or very similar to) C, C++, bash printf()
    # It's no longer the preferred way of formatting values, but you still see it a lot,
    # so it's good to be able to recognize it.
    # '%f' means 'format this as a floating point number
    #   (with a dot or comma, depending on locale).
    # There are a ton of other formats, that do things like print as an integer, print as hex,
    #   print in scientific notation (7.07e-1), print as a string, left-align, pad with zeros etc
    print("sin(0.5*pi) = %f" % math.sin(0.5 * math.pi))

    # Instead of printing the value directly, it's sometimes nicer to save it to a temp variable.
    # This can make the code more readable
    # Instead of using the '"%f" % value' syntax, I'm using the format function instead.
    # Format looks for matching brackets {} and subsitutes them with the arguments to format.
    mycos = math.cos(0.25 * math.pi)
    print("cos(0.25*pi) = {}".format(mycos))

    # With format, you can use {0}, {1}, {2} etc to explicitly refer to the first, second, 3rd ...
    #   argument to the format function.
    # This is useful for re-using the same argument more than once in the output, or
    #     re-arranging the format string without having to change the arguments
    print("log(e**2) = {1}, sqrt(16) = {0}".format(math.sqrt(16), math.log(math.e**2)))

    # You can also name the argument to be substituted explicitly, which can be handy
    #   if the format string is big, to reduce the chances of mistakes (for instance,
    #   if there's 17 arguments, deciding if you should use {12} or {13} by sight can be tricky).
    print("The greatest common denominator of 60 and 44 is {gcd}, and that's a fact"
          .format(gcd=math.gcd(60, 44)))


main()
