#!/usr/bin/env python3
import requests
import datetime
import subprocess
import tempfile
import sys

"""
This example illustrates the following new concepts:
* Using the 'requests' library for using web APIs
* Using datetime for date formatting
* Using a temporary file

(c) Frederik Van Bogaert, 2020. CC-BY-SA
"""

def main():
    current_comic_url = 'http://xkcd.com/info.0.json'
    cont = True

    while cont:
        request = requests.get(current_comic_url)
        if not request.ok:
            raise Exception("Failed to download comic meta-data")

        # The 'requests' library has a built-in JSON parser.
        # We can use that to extract all the useful data as a python dictionary
        comic_data = request.json()
        num = comic_data['num']
        title = comic_data['title']
        alt = comic_data['alt']
        date = datetime.date(year=int(comic_data['year']), month=int(comic_data['month']), day=int(comic_data['day']))
        print("Comic #{}".format(num))
        print("\tTitle:", title)
        print("\tDate:", date)
        print("\tAlt: ", alt)

        dl = requests.get(comic_data['img'])

        if not dl.ok:
            raise Exception("Failed to download comic image")

        with tempfile.NamedTemporaryFile() as comic:
            comic.write(dl.content)
            comic.flush()
            subprocess.run(['display', comic.name])

        key = ''
        while key not in ['P', 'p', 'N', 'n', 'X', 'x']:
            print("[P]rev - [N]ext - e[X]it")
            key = sys.stdin.read(1)
            if key in ['P', 'p']:
                num = num - 1
            elif key in ['N', 'n']:
                num = num + 1
            elif key.lower() == 'x':
                cont = False
            else:
                print("Please enter a valid key (p, n or x)")

        current_comic_url = 'http://xkcd.com/{}/info.0.json'.format(num)


if __name__ == '__main__':
    main()

